//
//  ContentView.swift
//  IncrementCounter
//  Created by brfsu on 21.01.2022.
//
import SwiftUI

struct ContentView: View
{
    @State var count : Int = 0
    
    var body: some View {
        Text("\(count)")
            .padding().font(.system(size: 40, weight: .bold))
        
        Button("Tap me!") {
            self.count += 1
            print("Current tap: \(count)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
