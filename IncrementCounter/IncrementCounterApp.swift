//
//  IncrementCounterApp.swift
//  IncrementCounter
//  Created by brfsu on 21.01.2022.
//
import SwiftUI

@main
struct IncrementCounterApp: App
{
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
